#!/usr/bin/env bash

# this script "installs" all the config files by making a symlink into
# the repository if a target file/symlink already exists, it asks
# about what to do

# syntax for where to place a config file is in its name.  the
# following rules apply: 
# 1. all target paths are relative to $HOME 
# 2. the target will have it's path prepended with a dot (.) 
# 3. every hash (#) in the config-files name in the repo is converted 
# to a slash (/) and the part before the slash is treated as a dirname

# so a file named "zshrc"'s target is $(HOME)/.zshrc, while ssh#config
# is symlinked to $(HOME)/.ssh/config

# todo: check for installed programs

# symlink a file (arg 1) to the path (arg 2)
# check if arg 2 exists, and if it does, ask if it should be backuped or skipped

# enable extra globbing operators
shopt -s extglob

function __linkFile()
{
    # have we got both path to file and to target link?
    if (( $# != 2 ))
    then
        return -1
    fi

    FROM_FILE="${1}"
    if [[ ! -e ${FROM_FILE} ]]
    then
        echo "file to symlink to does not exist!"
        return 2;
    fi

    TO_FILE="${2}"

    # if the file already exists, ask if the user want to skip this config or taking a backup
    if [[ -e ${TO_FILE} ]]
    then
        # check if the file is already symlinked
        if [[ -L "${TO_FILE}" && $(realpath "${TO_FILE}") == */"${FROM_FILE}" ]]
        then
            echo "${TO_FILE} is already symlinked, skipping..."
            return 0;
        fi

        echo -n "${TO_FILE} exists! do you want to backup and create symlink(y) or skip this one(n) (N/y): ";
        read -n 1 answer;
        echo
        if [[ "${answer}" != "y" ]]
        then
            echo "skipping ${TO_FILE}"
            return 0;
        fi

        echo "moving ${TO_FILE} to ${TO_FILE}_$(date +%Y-%m-%dT%T).bup..."
        mv "${TO_FILE}" "${TO_FILE}_$(date +%Y-%m-%dT%T).bup"

        # check if move succeded
        if (( $? != 0 ))
        then
            echo "making backup failed! aborting linking of ${TO_FILE}...";
            return 3
        fi
    fi
    
    # make the link! 
    echo "making symlink...";
    ln -sf "${FROM_FILE}" "${TO_FILE}"
    echo "${TO_FILE} is now in place!";
}

function __resolveTargetAndLink()
{
    # error if we didnt get argument
    if (( $# != 1 ))
    then
        echo "__resolveTargetAndLink() needs argument: config-file"
        return 1
    fi

    FROM_FILE="${1}"

    # assume all targets are relative to ~ and start with dot
    # test ([[) -f/-e does not expand tilde (~) that bitch!!!
    # use $HOME instead...
    TO_FILE="${HOME}/."
    TO_FILE+=${FROM_FILE//#//}
    __linkFile "${FROM_FILE}" "${TO_FILE}"
}

function __downloadExtensions()
{
    # download oh-my-zsh
    
    if [[ ! -e "${HOME}/.oh-my-zsh" ]]
    then
        echo -n "do you want to install oh-my-zsh to ~/.oh-my-zsh? (N/y):"
        read -n 1 answer
        echo
        if [[ "${answer}" == "y" ]]
        then
            echo "cloning oh-my-zsh into ~/.oh-my-zsh..."
            git clone git://github.com/robbyrussell/oh-my-zsh.git "${HOME}/.oh-my-zsh"
            echo "done!"
        else
            echo "not installing oh-my-zsh";
        fi
    else
        echo "~/.oh-my-zshrc exists, skipping installation..."
    fi
}

function __installAllConfigFiles()
{
    # go through all files in directory and install them
    # set Inter Field Separatorto newline
    OLDIFS="${IFS}"
    # magic...
    IFS=$'\n'

    # for all the files!
    for file in !(${0##*/})
    do
         __resolveTargetAndLink "${file}"
    done
    # restore IFS
    IFS="${OLDIFS}"
}

# cd to script location
cd ${0%/*}

# download and install extensions
__downloadExtensions

# install all configs
__installAllConfigFiles


