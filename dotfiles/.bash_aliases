    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'

# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

# ps aliases
alias psc='ps xawf -eo pid,user,cgroup,args'

# nginx aliases
# start
alias ngstart='sudo rc.d start nginx'
# restart
alias ngrestart='sudo rc.d restart nginx'
# stop
alias ngstop='sudo rc.d stop nginx'
# mount folders from vps
alias mount_play='sshfs ytvps08@ht:/var/play /mnt/ht/play'
alias mount_rubystack='sshfs ytvps08@ht:/opt/rubystack-2.1-0 /mnt/ht/rubystack'
# start play server (tilfeldige ord) - stop with ctrl+C
alias ord_start='ssh ht /opt/play/play start /var/play/tilfeldige-ord'
alias ord_stop='ssh ht /opt/play/play stop /var/play/tilfeldige-ord'

### play framework
# deploy on gae
alias play_gae_deploy='/usr/bin/play gae:deploy --gae=/home/floke/source/appengine-java-sdk-1.6.2.1/'

# adobe air
alias air='/opt/adobe-air-sdk/bin/adl -nodebug'
#/path/to/app/META-INF/AIR/application.xml'

# skype upside/down video fix
# https://bbs.archlinux.org/viewtopic.php?pid=979717#p979717
alias skype_video='LD_PRELOAD=/usr/lib32/libv4l/v4l1compat.so skype'

# mount smb shares
alias mount_archbox='sudo mount -t cifs //192.168.0.251/music /media/archbox/music/ -o user=floke,pass=majone5 && sudo mount -t cifs //192.168.0.251/video /media/archbox/video/ -o user=floke,pass=majone5'

# pacman aliases
alias pacman-sizesort="pacman -Qi | awk '/^Name/ {pkg=$3} /Size/ {print $4$5,pkg}' | sort -n"
alias pacman-clean='sudo pacman -Rsn $(pacman -Qqdt)'
