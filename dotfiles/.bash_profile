#
# ~/.bash_profile
#

[[ -f ~/.bashrc ]] && . ~/.bashrc

# rvm
#[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm"
#[[ -s "/usr/local/rvm/src/rvm/scripts/rvm" ]] && 
source "/usr/local/rvm/src/rvm/scripts/rvm"

# start ssh-agent
eval $(ssh-agent)

# map keyboard to toggle languages
setxkbmap -layout us,no -variant -option grp:alt_shift_toggle
