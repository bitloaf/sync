# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=10000
SAVEHIST=10000
bindkey -v
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/floke/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall

# add bin folder to path
path=(~/sync/bin $path)
# add play to path
path=(~/bin/play $path)

# aliases
alias ls='ls --color=auto'
alias l='ls -CF'
alias la='ls -A'
alias ll='ls -alF'
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'
# http://stackoverflow.com/a/8084186
alias git-submodules-update='git pull --recurse-submodules && git submodule update --recursive'
# pacman aliases
alias pacman-sizesort='pacman -Qi | awk "/^Name/ {pkg=$3} /Size/ {print $4$5,pkg}" | sort -nr | less'
alias pacman-upgrade='sudo pacman -Syu'        # Synchronize with repositories before upgrading packages that are out of date on the local system.
alias pacman-install='sudo pacman -S'           # Install specific package(s) from the repositories
alias pacman-install-fromfile='sudo pacman -U'          # Install specific package not from the repositories but from a file 
alias pacman-remove='sudo pacman -R'           # Remove the specified package(s), retaining its configuration(s) and required dependencies
alias pacman-removeall='sudo pacman -Rns'        # Remove the specified package(s), its configuration(s) and unneeded dependencies
alias pacman-info='pacman -Si'              # Display information about a given package in the repositories
alias pacman-search='pacman -Ss'             # Search for package(s) in the repositories
alias pacman-info-local='pacman -Qi'              # Display information about a given package in the local database
alias pacman-search-local='pacman -Qs'             # Search for package(s) in the local database
alias pacman-update='sudo pacman -Sy && sudo abs'     # Update and refresh the local package and ABS databases against repositories
alias pacman-install-deps='sudo pacman -S --asdeps'        # Install given package(s) as dependencies of another package
alias pacman-refresh='sudo pacman -Syy'                # Force refresh of all package lists after updating /etc/pacman.d/mirrorlist

# file aliases
alias sizesort='du -sm * | sort -nr | less'

# keyboard shortcuts taken from https://wiki.archlinux.org/index.php/Zsh#Key_bindings
# create a zkbd compatible hash;
# to add other keys to this hash, see: man 5 terminfo
typeset -A key

key[Home]=${terminfo[khome]}

key[End]=${terminfo[kend]}
key[Insert]=${terminfo[kich1]}
key[Delete]=${terminfo[kdch1]}
key[Up]=${terminfo[kcuu1]}
key[Down]=${terminfo[kcud1]}
key[Left]=${terminfo[kcub1]}
key[Right]=${terminfo[kcuf1]}
key[PageUp]=${terminfo[kpp]}
key[PageDown]=${terminfo[knp]}

# setup key accordingly
[[ -n "${key[Home]}"     ]]  && bindkey  "${key[Home]}"     beginning-of-line
[[ -n "${key[End]}"      ]]  && bindkey  "${key[End]}"      end-of-line
[[ -n "${key[Insert]}"   ]]  && bindkey  "${key[Insert]}"   overwrite-mode
[[ -n "${key[Delete]}"   ]]  && bindkey  "${key[Delete]}"   delete-char
[[ -n "${key[Up]}"       ]]  && bindkey  "${key[Up]}"       up-line-or-history
[[ -n "${key[Down]}"     ]]  && bindkey  "${key[Down]}"     down-line-or-history
[[ -n "${key[Left]}"     ]]  && bindkey  "${key[Left]}"     backward-char
[[ -n "${key[Right]}"    ]]  && bindkey  "${key[Right]}"    forward-char
[[ -n "${key[PageUp]}"   ]]  && bindkey  "${key[PageUp]}"   beginning-of-buffer-or-history
[[ -n "${key[PageDown]}" ]]  && bindkey  "${key[PageDown]}" end-of-buffer-or-history

# Finally, make sure the terminal is in application mode, when zle is
# active. Only then are the values from $terminfo valid.
if (( ${+terminfo[smkx]} )) && (( ${+terminfo[rmkx]} )); then
    function zle-line-init () {
        printf '%s' "${terminfo[smkx]}"
    }
    function zle-line-finish () {
        printf '%s' "${terminfo[rmkx]}"
    }
    zle -N zle-line-init
    zle -N zle-line-finish
fi

# load colors
autoload -U promptinit
promptinit
autoload -U colors && colors

# vcs_info
# https://github.com/zsh-users/zsh/blob/master/Misc/vcs_info-examples
# http://stackoverflow.com/a/12935606

# load vcs_info
setopt prompt_subst
autoload -Uz vcs_info

zstyle ':vcs_info:*' enable git
zstyle ':vcs_info:*' check-for-changes true
zstyle ':vcs_info:*' stagedstr '%F{40}•%f'
zstyle ':vcs_info:*' unstagedstr '%F{214}•%f'
zstyle ':vcs_info:*' actionformats \
    '%F{5}[%F{2}%b%F{3}|%F{1}%a%F{5}]%f'
zstyle ':vcs_info:*' formats       \
    '%F{5}[%F{2}%b%F{5}]%f'
zstyle ':vcs_info:(sv[nk]|bzr):*' branchformat '%b%F{1}:%F{3}%r'
#zstyle ':vcs_info:git+set-message:*' hooks check-untracked
zstyle ':vcs_info:git*+set-message:*' hooks git-st

# Run all the prompt hook functions
# (stolen, wholesale, from the excellent hook system in vcs_info)
function pr_run_hooks() {
    local debug hook func
    local -a hooks

    zstyle -g hooks ":pr_nethack:*" hooks
    zstyle -t ":pr_nethack:*" debug && debug=1 || debug=0

    (( ${#hooks} == 0 )) && return 0

    for hook in ${hooks} ; do
        func="+pr-${hook}"
        if (( ${+functions[$func]} == 0 )); then
            (( debug )) && printf '  + Unknown function: "%s"\n' "${func}"
            continue
        fi
        (( debug )) && printf '  + Running function: "%s"\n' "${func}"
        true
        ${func} "$@"
        case $? in
            (0)
                ;;
            (*)
                break
                ;;
        esac
    done
}

function +vi-check-untracked() {
    [[ -n $(git ls-files --others --exclude-standard 2>&-) ]] &&
    hook_com[unstaged]="${hook_com[unstaged]}%F{red}•"
}

# http://eseth.org/2010/git-in-zsh.html
# Show remote ref name and number of commits ahead-of or behind
function +vi-git-st() {
    local ahead behind remote
    local -a gitstatus

    # Are we on a remote-tracking branch?
    remote=${$(git rev-parse --verify ${hook_com[branch]}@{upstream} \
        --symbolic-full-name 2>/dev/null)/refs\/remotes\/}

    if [[ -n ${remote} ]] ; then
        # for git prior to 1.7
        # ahead=$(git rev-list origin/${hook_com[branch]}..HEAD | wc -l)
        ahead=$(git rev-list ${hook_com[branch]}@{upstream}..HEAD 2>/dev/null | wc -l)
        (( $ahead )) && gitstatus+=( "${c3}+${ahead}${c2}" )

        # for git prior to 1.7
        # behind=$(git rev-list HEAD..origin/${hook_com[branch]} | wc -l)
        behind=$(git rev-list HEAD..${hook_com[branch]}@{upstream} 2>/dev/null | wc -l)
        (( $behind )) && gitstatus+=( "${c4}-${behind}${c2}" )

        hook_com[branch]="${hook_com[branch]} [${(j:/:)gitstatus}]"
    fi
}

precmd() {
  vcs_info
#  if [ -n "$vcs_info_msg_0_" ]; then
#    echo "%{$fg[grey]%}${vcs_info_msg_0_}%{$reset_color%}$del"
#  fi
  pr_run_hooks
}

case $HOST in
  archtop)
    hostcolor=green ;;
  vps)
    hostcolor=red ;;
  archbox)
    hostcolor=magenta ;;
<<<<<<< HEAD
  raspbmc)
=======
  ubox)
    hostcolor=magenta ;;
  xbmc)
>>>>>>> 6e90eae507b8f1bbad254f80271d6bcf4f1aa363
    hostcolor=purple ;;
  *)
    hostcolor=white ;;
esac

[[ -z $SSH_CONNECTION ]] && dollar=blue || dollar=red;

PROMPT="%F{green}%n@%F{$hostcolor}%m%f:%F{yellow}%~ %F{$dollar}\$%f "
RPROMPT='${vcs_info_msg_0_}'

# functions
# stolen from https://github.com/Osse/dotfiles/blog/master/.zshrc#L179
function sprunge() {
    curl -sF 'sprunge=<-' http://sprunge.us | tr -d '\n'
    echo $output${1:+\?$1}
}
