#!/usr/bin/env bash

# installs needed packages listed in files

# run as root
if [[ $(/usr/bin/id -u) -ne 0 ]];
then
	echo "Not running as root" >&2;
	exit 1;
fi

# checks if files exists
FILE="CLIPACKAGES"
if [[ ! -e $FILE ]];
then
	echo "The file '$FILE' does not exist" >&2;
	exit 1;
fi

# installs packages from file
PACKAGES=$( grep "^[a-z0-9]" $FILE );
pacman --noconfirm --needed -Sy $PACKAGES

# set zshell as default shell
chsh -s $(which zsh) $SUDO_USER
